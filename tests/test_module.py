# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class TestCase(ModuleTestCase):
    """Test module"""
    module = 'stock_move_done2cancel'

    def setUp(self):
        super(TestCase, self).setUp()


del ModuleTestCase

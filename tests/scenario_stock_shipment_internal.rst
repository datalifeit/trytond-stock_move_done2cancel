================================
Stock Shipment Internal Scenario
================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)
    >>> tomorrow = today + relativedelta(days=1)

Install stock Module::

    >>> config = activate_modules('stock_move_done2cancel')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()

Create stock user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> stock_user = User()
    >>> stock_user.name = 'Stock'
    >>> stock_user.login = 'stock'
    >>> stock_user.company = company
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> stock_user.groups.append(stock_group)
    >>> stock_user.save()

Create Internal Shipment from lost_found location::

    >>> user = config.user
    >>> config.user = stock_user.id
    >>> Shipment = Model.get('stock.shipment.internal')
    >>> StockMove = Model.get('stock.move')
    >>> lost_found_shipment = Shipment()
    >>> lost_found_shipment.planned_date = today
    >>> lost_found_shipment.company = company
    >>> lost_found_shipment.from_location = lost_found_loc
    >>> lost_found_shipment.to_location = internal_loc
    >>> move = StockMove()
    >>> move = lost_found_shipment.moves.new()
    >>> move.product = product
    >>> move.unit = unit
    >>> move.quantity = 2
    >>> move.from_location = lost_found_loc
    >>> move.to_location = internal_loc
    >>> move.currency = company.currency
    >>> lost_found_shipment.click('wait')
    >>> lost_found_shipment.click('assign_try')
    >>> lost_found_shipment.state
    'assigned'
    >>> lost_found_shipment.click('done')
    >>> lost_found_shipment.state
    'done'

Try to cancel::

    >>> lost_found_shipment.click('cancel') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    ...
    UserError: ('UserError', ('Cannot Cancel Internal Shipment in Done state.', ''))
    >>> stock_group, = Group.find([
    ...     ('name', '=', 'Stock Force Cancel internal shipment')])
    >>> config.user = user
    >>> stock_user.groups.append(stock_group)
    >>> stock_user.save()
    >>> config.user = stock_user.id
    >>> lost_found_shipment.click('cancel')
    >>> lost_found_shipment.state
    'cancelled'
    >>> list(set(m.state for m in lost_found_shipment.moves))
    ['cancelled']
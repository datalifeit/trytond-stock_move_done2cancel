===========================
Stock Shipment Out Scenario
===========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock Module::

    >>> config = activate_modules('stock_move_done2cancel')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create stock user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> stock_user = User()
    >>> stock_user.name = 'Stock'
    >>> stock_user.login = 'stock'
    >>> stock_user.company = company
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> stock_user.groups.append(stock_group)
    >>> stock_user.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create Move and cancel::

    >>> admin_user = config.user
    >>> set_user(stock_user)
    >>> StockMove = Model.get('stock.move')
    >>> _move = StockMove()
    >>> _move.product = product
    >>> _move.unit = unit
    >>> _move.quantity = 1
    >>> _move.from_location = supplier_loc
    >>> _move.to_location = storage_loc
    >>> _move.planned_date = today
    >>> _move.effective_date = today
    >>> _move.company = company
    >>> _move.unit_price = Decimal('1')
    >>> _move.currency = company.currency
    >>> _move.click('do')
    >>> bool(_move.cancelation_allowed)
    True
    >>> _move.click('cancel')
    >>> _move.state
    'cancelled'

Make 1 unit of the product available::

    >>> incoming_move = StockMove()
    >>> incoming_move.product = product
    >>> incoming_move.unit = unit
    >>> incoming_move.quantity = 1
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('1')
    >>> incoming_move.currency = company.currency
    >>> incoming_move.click('do')

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> move = shipment_out.outgoing_moves.new()
    >>> move.product = product
    >>> move.unit = unit
    >>> move.quantity = 1
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_out.save()
    >>> shipment_out.click('wait')
    >>> shipment_out.click('assign_try')
    >>> shipment_out.click('pick')
    >>> shipment_out.click('pack')
    >>> shipment_out.click('done')
    >>> shipment_out.state
    'done'

Check if move can be cancel::

    >>> bool(shipment_out.outgoing_moves[0].cancelation_allowed)
    False

Check revert::

    >>> shipment_out.reload()
    >>> _move, = shipment_out.outgoing_moves
    >>> _move.state
    'done'
    >>> _move.click('cancel')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: You cannot cancel stock moves in state Done with origin. - 
    >>> _move.state
    'done'
    >>> set_user(admin_user)
    >>> _move.click('cancel')
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: You cannot cancel stock moves in state Done with origin. - 
    >>> set_user(stock_user)
    >>> shipment_out.click('cancel')
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot Cancel Shipment Out in Done state. - 
    >>> set_user(admin_user)
    >>> shipment_out.click('cancel')
    >>> _move.reload()
    >>> _move.state
    'cancelled'
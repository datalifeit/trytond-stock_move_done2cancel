# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .move import (Move, ShipmentOut, ShipmentOutReturn, ShipmentIn,
    ShipmentInReturn, ShipmentInternal)


def register():
    Pool.register(
        Move,
        ShipmentOut,
        ShipmentOutReturn,
        ShipmentIn,
        ShipmentInReturn,
        ShipmentInternal,
        module='stock_move_done2cancel', type_='model')
